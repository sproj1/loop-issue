<!DOCTYPE>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>

<body>
  <?php
    var_dump($_POST);
    
    function extractGradeFromMarks($grade) {
      if ($grade >= 90)
          return "A";
      elseif ($grade >= 80 && $grade <= 89)
          return "B";
      elseif ($grade >= 70 && $grade <= 79)
          return "C";
      elseif ($grade >= 60 && $grade <= 69)
          return "D";
      else
          return "F";
    }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Handle the form.
    if (isset($_POST['submit'])) {

      $dbc = mysqli_connect('localhost', 'root', '', 'freose');
          
      if (!$dbc){
          die('could not connect:'.mysqli_error());
      }

      $query = "SELECT * FROM students";

      $data = mysqli_query($dbc, $query);

      $rowcount = (int) mysqli_num_rows($data);

      //if(mysqli_num_rows($data)>0)

      //Displaying student grades detail on a webpage using html table
      echo '<table table border = "1px">';
      echo '<tr><th>STUDENT ID</th><th>STUDENT NAME</th><th>ORIGINAL NUMBER GRADE</th><th>ORIGINAL LETTER GRADE</th><th>ADJUSTED GRADE</th><th>ADJUSTED LETTER GRADE</th><th>START DATE</th><th>COMPLETION DATE</th><th>DAYS IN CLASS</th></tr>';

      $total = 0;

      while ($row = mysqli_fetch_assoc($data)){ 
                                                
        $sid = $row["student_id"];
        $name = $row["student_name"];
        $grade = $row["student_grade"];
        $start =$row["start_date"];
        $end = $row["completion_date"];     
    
        // echo '<td>'.extractGradeFromMarks($grade).'</td>';
          $adjusted_grade = $grade + ($grade*0.1); //Calculate an adjusted grade for each student by adding 10%.
        // echo '<td>'.$adjusted_grade.'</td>';
        // echo '<td>'.extractGradeFromMarks($adjusted_grade).'</td>';
          
          $num_days = (abs(strtotime($end) - strtotime($start))) / 86400; //Calculate an adjusted grade for each student by adding 10%.
        // echo '<td>'.$num_days.'</td>';
          
        // Adding(summing up) grades to total variable to get total grades.
          $total += $row['student_grade'];

          
        // foreach($data as $key => $value) {
        // echo '<tr>';
          echo '<td>'. $sid .'</td>';
          echo '<td>'. $name .'</td>';
          echo '<td>'. $grade .'</td>';
          echo '<td>'.extractGradeFromMarks($grade).'</td>';
          echo '<td>'.$adjusted_grade.'</td>';  
          echo '<td>'.extractGradeFromMarks($adjusted_grade).'</td>';
          echo '<td>'. $start .'</td>';
          echo '<td>'. $end .'</td>';
          echo '<td>'.$num_days.'</td>';        
        // echo '</tr>';
        // }
        // $total += $grade; //Adding(summing up) grades to total variable to get total grades.
      }
      
      echo '<tr><td><b>Total Number of students:</b></td>';
      echo '<td colspan="4">'.$rowcount.'</td></tr>'; // Number of students will be length of $studentGrades array.
      echo '<tr><td><b>Total Grade (Original):</b></td>';
      //Displaying total grades.
      echo '<td colspan="4">'.$total.'</td></tr>';
      echo '<tr><td><b>Average Grade (Original):</b></td>';
      //Getting average grade.
      echo '<td colspan="4">'.$total/$rowcount.'</td></tr>';
      echo '</table>';
      echo '<br/>';

      //Exiting application.
      exit();
      mysqli_close($dbc); // Close the connection.
    }
  }

      echo "$total";

  ?>

</body>

</html>
